/* Extra runs conceded per team in the year 2016 */

/**
 * 1. from matchData, filter out matches associated with the given year
 * 2. reduce it to array of ids
 * 3. use this array to filter out delivers
 * 4. 
 * 
 */


function extraRunsPerTeamForGivenYearFunc(matchData, deliveryData, year) {
    let matchIdArray = matchData // find IDs of matches held that year
                        .reduce((idArray, match) => {
                            if(match.season === year) {
                                idArray.push(match.id);
                            }
                            return idArray;
                        }, []);
    
    return deliveryData
            .reduce((finalObj, delivery) => {
                if(matchIdArray.includes(delivery.match_id)) {
                    if(finalObj[delivery.bowling_team] === undefined) {
                        finalObj[delivery.bowling_team] = 0;
                    }
                    finalObj[delivery.bowling_team] += +delivery.extra_runs;
                }
                return finalObj;
            }, {});
    
}

module.exports = extraRunsPerTeamForGivenYearFunc;



/**\
 * Delivery data sample
 * {
  match_id: '1',
  inning: '1',
  batting_team: 'Sunrisers Hyderabad',
  bowling_team: 'Royal Challengers Bangalore',
  over: '1',
  ball: '1',
  batsman: 'DA Warner',
  non_striker: 'S Dhawan',
  bowler: 'TS Mills',
  is_super_over: '0',
  wide_runs: '0',
  bye_runs: '0',
  legbye_runs: '0',
  noball_runs: '0',
  penalty_runs: '0',
  batsman_runs: '0',
  extra_runs: '0',
  total_runs: '0',
  player_dismissed: '',
  dismissal_kind: '',
  fielder: ''
}
 */

/**
 * match data sample
 * {
    id: '51',
    season: '2017',
    city: 'Delhi',
    date: '2017-05-12',
    team1: 'Delhi Daredevils',
    team2: 'Rising Pune Supergiant',
    toss_winner: 'Delhi Daredevils',
    toss_decision: 'bat',
    result: 'normal',
    dl_applied: '0',
    winner: 'Delhi Daredevils',
    win_by_runs: '7',
    win_by_wickets: '0',
    player_of_match: 'KK Nair',
    venue: 'Feroz Shah Kotla',
    umpire1: 'KN Ananthapadmanabhan',
    umpire2: 'CK Nandan',
    umpire3: ''
  }
 */
