/* Number of matches won per team per year in IPL. */

function matchesWonPerTeamPerYearFunc(data) {
    return data.reduce((finalData, match) => {
        
        if(finalData[match.season] === undefined) {
            finalData[match.season] = {};
        }
        
        if(finalData[match.season][match.winner] === undefined) {
            finalData[match.season][match.winner] = 0; 
        }
        
        finalData[match.season][match.winner]++;
        return finalData;
    }, {});
}

module.exports = matchesWonPerTeamPerYearFunc;