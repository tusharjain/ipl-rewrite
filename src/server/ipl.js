/*

In this data assignment you will transform raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016
4. Top 10 economical bowlers in the year 2015

Implement the 4 functions, one for each task. Use the results of the functions to dump JSON files in the output folder

*/


const fs = require("fs");
const csvtojson = require("csvtojson");

const matchesPerYearFunc = require("./modules/matchesPerYear.js");
const matchesWonPerTeamPerYearFunc = require("./modules/matchesWonPerTeamPerYear.js");
const extraRunsPerTeamForGivenYearFunc = require("./modules/extraRunsPerTeamForGivenYear.js");
const top10EconomicBowlersForGivenYearFunc = require("./modules/top10EconomicBowlersForGivenYear.js");

const matchesPerYearFile = '../public/output/matchesPerYear.json';
const matchesWonPerTeamPerYearFile = '../public/output/matchesWonPerTeamPerYear.json';
const extraRunsPerTeamForGivenYearFile = '../public/output/extraRunsPerTeamForGivenYear.json';
const top10EconomicBowlersForGivenYearFile = '../public/output/top10EconomicBowlersForGivenYear.json';

const matchDataFile = '../data/matches.csv';
const deliveryDataFile = '../data/deliveries.csv';

csvtojson()
  .fromFile(matchDataFile)
  .then(matchData => {
    saveData(matchesPerYearFunc(matchData), matchesPerYearFile);
    saveData(matchesWonPerTeamPerYearFunc(matchData), matchesWonPerTeamPerYearFile); 

    csvtojson()
      .fromFile(deliveryDataFile)
      .then(deliveryData => {
        saveData(extraRunsPerTeamForGivenYearFunc(matchData, deliveryData, '2016'), extraRunsPerTeamForGivenYearFile);
        saveData(top10EconomicBowlersForGivenYearFunc(matchData, deliveryData, '2015'), top10EconomicBowlersForGivenYearFile);
      });
  });


function saveData(data, filepath) {
  data = JSON.stringify(data, null, 2); 

  fs.writeFile(filepath, data, (err) => {
    if(err) {
      console.error(err);
      return;
    }
  });
}



