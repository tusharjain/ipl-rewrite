/* Number of matches played per year for all the years in IPL. */


function matchesPerYearFunc(data) {
    return data.reduce((finalData, match) => {
                    if(finalData[match.season] === undefined) {
                        finalData[match.season] = 0; 
                    }
                    finalData[match.season]++;
                    return finalData; 
                }, {});
}

module.exports = matchesPerYearFunc; 

