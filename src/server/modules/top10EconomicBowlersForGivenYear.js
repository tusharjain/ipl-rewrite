/** Top 10 economical bowlers in the year 2015 */

function top10EconomicBowlersForGivenYearFunc(matchData, deliveryData, year) {
    let matchIdArray = matchData // find IDs of matches held that year
                        .reduce((idArray, match) => {
                            if(match.season === year) {
                                idArray.push(match.id);
                            }
                            return idArray;
                        }, []);

   return deliveryData 
            .filter((delivery) => matchIdArray.includes(delivery.match_id)) // deliveries for match_ids of that year
            .reduce((bowlerDataTemp, delivery) => {                         // cumulative stats of each bowler - runs, deliveries, economy
                let bowler = bowlerDataTemp.find(bowler => bowler.name === delivery.bowler);
                
                if(bowler === undefined) {
                    bowlerDataTemp.push({ 'name': delivery.bowler, 'total_runs': 0, 'deliveries': 0, 'economy': 0 }); 
                    bowler = bowlerDataTemp[bowlerDataTemp.length - 1];
                }

                bowler['total_runs'] += +delivery.total_runs;
                bowler['deliveries']++; 
                bowler['economy'] = +(bowler['total_runs'] / (bowler['deliveries'] / 6)).toFixed(3);
                
                return bowlerDataTemp;
                /* using array, instead of object, to keep record so that it can be sorted later. */
            }, []) 
            .sort((a, b) => {                                               // sort ascending - the less the economy, the better the bowler
                return a.economy - b.economy;
            })
            .slice(0, 10)                                                   // top 10 performers
            .reduce((top10temp, bowler) => {                                // final obj with only the economy data
                if(top10temp[bowler.name] === undefined) {
                    top10temp[bowler.name] = bowler.economy; 
                }
                return top10temp;
            }, {});
}

module.exports = top10EconomicBowlersForGivenYearFunc;


/**\
 * Delivery data sample
 * {
  match_id: '1',
  inning: '1',
  batting_team: 'Sunrisers Hyderabad',
  bowling_team: 'Royal Challengers Bangalore',
  over: '1',
  ball: '1',
  batsman: 'DA Warner',
  non_striker: 'S Dhawan',
  bowler: 'TS Mills',
  is_super_over: '0',
  wide_runs: '0',
  bye_runs: '0',
  legbye_runs: '0',
  noball_runs: '0',
  penalty_runs: '0',
  batsman_runs: '0',
  extra_runs: '0',
  total_runs: '0',
  player_dismissed: '',
  dismissal_kind: '',
  fielder: ''
}
 */

/**
 * match data sample
 * {
    id: '51',
    season: '2017',
    city: 'Delhi',
    date: '2017-05-12',
    team1: 'Delhi Daredevils',
    team2: 'Rising Pune Supergiant',
    toss_winner: 'Delhi Daredevils',
    toss_decision: 'bat',
    result: 'normal',
    dl_applied: '0',
    winner: 'Delhi Daredevils',
    win_by_runs: '7',
    win_by_wickets: '0',
    player_of_match: 'KK Nair',
    venue: 'Feroz Shah Kotla',
    umpire1: 'KN Ananthapadmanabhan',
    umpire2: 'CK Nandan',
    umpire3: ''
  }
 */
